+++
title="Making Your Electronics Tamper-Evident"
date=2023-04-01

[taxonomies]
categories = ["Defensive"]
tags = ["opsec", "easy"]

[extra]
blogimage="/images/X230.jpg"
toc=true
+++

If police can ever have [physical access](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance/physical-access.html) to an electronic device like a laptop, even [for five minutes](https://www.vice.com/en/article/a3q374/hacker-bios-firmware-backdoor-evil-maid-attack-laptop-5-minutes), they can install hardware keyloggers, create images of the storage media, or otherwise trivially compromise it on the hardware, firmware, or software level. One way to minimize this risk is to make it tamper-evident. As the CSRC Threat Library [notes](https://www.csrc.link/threat-library/mitigations/tamper-evident-preparation.html), "Tamper-evident preparation will make it possible to discern when something has been [physically accessed](/glossary/#physical-attacks) - it's not possible to prevent a powerful enemy from obtaining physical access to your computer when you are away, but it should be possible to be able to detect when they do."

<!-- more -->
['Evil maid' attacks](https://en.wikipedia.org/wiki/Evil_maid_attack) work like this: An attacker gains temporary access to your [encrypted](/glossary/#encryption) laptop or phone. Although they can’t decrypt your data, they can spend a few minutes tampering with your laptop and then leave it exactly where they found it. When you come back and type in your credentials, you have been hacked. The attacker could have [modified data on your hard disk](https://media.ccc.de/v/gpn20-32-poc-implementing-evil-maid-attack-on-encrypted-boot), replaced the firmware, or installed a hardware component like a keylogger.  

# Tamper-Evident Laptop Screws 

Let's start with your laptop. For a seal to be effective at alerting you to intruders, it needs to be impossible to remove and replace without leaving a mark, and also unique—otherwise the adversary could just replicate the seal and you’d never know they’d been there. Glitter nail polish will form a unique pattern that is impossible to replicate, and if you take a photo of this pattern, you can use it to verify that the nail polish has not been removed and then reapplied in your absence, such as during a [covert house search](https://www.csrc.link/threat-library/techniques/covert-house-search.html). The presentation "[Thwarting Evil Maid Attacks](https://media.ccc.de/v/30C3_-_5600_-_en_-_saal_1_-_201312301245_-_thwarting_evil_maid_attacks_-_eric_michaud_-_ryan_lackey)" introduced this technique in 2013. 

Mullvad VPN [made a guide](https://mullvad.net/en/help/how-tamper-protect-laptop/) for applying this technique: first apply stickers over the laptop chassis screws, then the nail polish. An [independent test](https://dys2p.com/en/2021-12-tamper-evident-protection.html#glitzer-nagellack-mit-aufklebern) noted:
> Attackers without a lot of practice can use a needle or scalpel, for example, to drive under the sticker and push it partially upward to get to the screws relatively easily. The broken areas in the paint could be repaired with clear nail polish, although we did not need to do this in most of our tests. The picture below is a pre-post-comparison of one of our first attempts. Except for 3-4 glitter elements at the top left edge of the sticker, all others are still in the same place. This could be further reduced in subsequent attempts, so we rate this method as only partially suitable. [...] The relevant factor in this process is the amount of elements on the edge of the sticker. In addition, there are special seal stickers available which break when peeled off. They are probably more suitable for this method.

![mullvad](mullvad.png)

For this reason, it is preferable to apply nail polish directly to the screws instead of on top of a sticker. This direct application is done for [NitroKey](https://docs.nitrokey.com/nitropad/qubes/sealed-hardware) and [Purism](https://puri.sm/posts/anti-interdiction-update-six-month-retrospective/) laptops. Keep these nuances in mind:
> The screws holes are particularly relevant here. If they are too deep, it is difficult to take a suitable photo of the seal under normal conditions. If the hole is shallow or if it is completely filled with nail polish, there is a risk that if a lot of polish is used, the top layer can be cut off and reapplied after manipulation with clear polish. If the nail polish contains too few elements, they could be manually arranged back to the original location after manipulation if necessary.

![X230](X230.jpg)

Glitter nail polish was successfully bypassed during a Tamper Evident Challenge in 2018 - the winner [explained](https://hoodiepony.medium.com/bypassing-the-glitter-nail-polish-tamper-evident-seal-25d6973d617d) how they managed to succeed. It is worth noting that a nail polish brand was used with relatively large glitter pieces of only two colors. It would be difficult to apply this bypass to inset screw holes; if the glitter was painted on with a high density of elements, but not too thickly, that would also increase the difficulty. Finally, [using an adhesive](https://dys2p.com/en/2021-12-tamper-evident-protection.html#glitzer-nagellack-mit-klebstoff) would also make the bypass less feasible. 

Verification that the random pattern hasn't changed can be done manually with what astronomers called "blink comparison". This is used in astronomy to detect small changes in the night sky: you rapidly flick between the original photo and the current one, which makes it easier to see any changes. Alternatively, if you have an Android smartphone (either [GrapheneOS](/posts/grapheneos/) or a cheap one for [intrusion detection](#physical-intrusion-detection) which will have an inferior camera) you can use an app called [Blink Comparison](https://github.com/proninyaroslav/blink-comparison) which makes it less likely to miss something. It can be installed like you would for any [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software), which is to say, not through F-Droid. 

The Blink Comparison app encrypts its storage, to prevent an adversary from easily replacing the photos, and gives a helpful interface for comparing them. The app helps you to take the comparison photo from the same angle and distance as the original photo. Blink Comparison then switches between the two images when the screen is touched, making direct comparison much easier. 

Now that you understand the nuances of using nail polish on the chassis screws of your laptop(s), we'll actually do it - this is best done after [flashing HEADS](#tamper-evident-software-and-firmware) so that it doesn't have to be removed and repeated. Before getting started, you can also take a photo of the inside of the laptop, in case one day you need to check if its internal components have been tampered with despite the nail polish protection (keeping in mind that not all components are visible). Use a nail polish that has different colors and sizes of glitter, like that shown above. 
* First, take a photo of the underside of the computer and use a software like GIMP to number the screws, in order to make it easier to verify. For example, the ThinkPad X230 shown above has 13 screws which need to be numbered so that in the future you know which screw the photo `3.jpg` refers to. 
* Apply the glitter nail polish directly to each screw, with sufficient density of glitter but not too thickly. 
* Once it has dried, be sure to take good close-up photos of each individual screw - either with the Blink Comparison app on a smartphone, or with a normal camera. It is a good idea to use lighting that is reproducible, so close the blinds on any windows and rely on the indoor lighting and camera flash. Number the file names of the photos, and back them up to a second storage location. 

If you ever need to remove the nail polish to access the internal of the laptop, you can use a syringe to apply the nail polish remover so as to avoid putting too much and damaging the internal electronics. 

# Tamper-Evident Storage 

Now that you understand the concept, you need a tamper-evident storage solution for all sensitive electronics when you are out of the house (laptops, external drives, USBs, phones, external keyboards, and mice). Safes are often used to protect valuable items, but they can be bypassed in several ways, and some of these bypasses are difficult to detect (see the [Appendix](#appendix-cracking-safes)). It is not trivial or inexpensive to make a safe tamper-evident, if it can be done at all. 

TODO photo 

A better and cheaper solution is to implement the guide of [dys2p](https://dys2p.com/en/2021-12-tamper-evident-protection.html#kurzzeitige-lagerung):
>  When we need to leave a place and leave items or equipment behind, we can store them in a box that is transparent from all sides. Then we fill the box with our colorful mixture so that our devices are covered. The box should be stored in such a way that shocks or other factors do not change the mosaic. For example, the box can be positioned on a towel or piece of clothing on an object in such a way that this attenuates minor vibrations of the environment, but the box cannot slide off it.
>
>For an overall comparison, we can photograph the box from all visible sides and store these photos on a device that is as secure as possible, send it to a trusted person via an encrypted and verified channel, or send it to another device of our own. The next step is to compare the found mosaic with the original one. The app Blink Comparison is ideal for this purpose.
>
>To protect an object from damage, e.g., by staining or by the substance leaking into, say, the ports of a laptop, it can be wrapped in cling film, a bag, or otherwise.

Several colorful mixtures are described: [red lentils & beluga lentils](https://dys2p.com/en/2021-12-tamper-evident-protection.html#rote-linsen-und-belugalinsen), [yellow peas & white beans](https://dys2p.com/en/2021-12-tamper-evident-protection.html#gelbe-erbsen-und-wei%C3%9Fe-bohnen), etc. For a box that is transparent on all sides and can fit a laptop, a small fish tank works well. [Longer-term storage](https://dys2p.com/en/2021-12-tamper-evident-protection.html#laengerfristige-lagerung-oder-versand) can use vacuum seals. 

This excerpted instruction assumes that we take the cellphone with us, but [as discussed elsewhere](/posts/nophones/#do-you-really-need-a-phone), this has its own security issues and so is not recommended. So the smartphone that we use to take a photo of the storage will need to stay in the house out of storage. [In the next section](#physical-intrusion-detection), we recommend that you acquire a cheap Android phone that only runs an app called Haven when you are out of the house. This device is going to stay out of storage anyway, so you can use it to take photos of the storage. Alternatively, if you don't have a dedicated Haven phone but you do have a [GrapheneOS](/posts/grapheneos/) device (or if the Haven phone camera is too low-quality), you can use it to take photos of the storage and then hide it somewhere in your home while you are away. With no phone, a camera can be used. Cameras, however, don't have encryption, so modifying the photos is significantly easier. 

<details>
<summary><strong>If you use a dedicated Haven phone</strong></summary>
</details>

* Once you have put bagged electronic devices in the container, and covered them with a colorful mixture, take the photos on this Haven phone using the Blink Comparison app. Send them to yourself over [Signal](/posts/e2ee/#signal) (with the Note to Self feature, and delete for everyone) or [Element](/posts/e2ee/#element-matrix). 
* Once you return, do the verification on the Haven phone using Blink Comparison. 
* Once your devices are out of storage, check that the images you sent to yourself on Signal/Element aren't different from those on your Haven phone, and also check the timestamp. Once the verification is complete, you can delete the photos so that there is no confusion in future verifications about which photos to use. 

<details>
<summary><strong>If you use a GrapheneOS phone, but not a dedicated Haven phone</strong></summary>
</details>

* Once you have put bagged electronic devices in the container, and covered them with a colorful mixture, take the photos using the Blink Comparison app. Send them to yourself over [Signal](/posts/e2ee/#signal) (with the Note to Self feature, and delete for everyone) or [Element](/posts/e2ee/#element-matrix). Power off the device and hide it somewhere. 
* Once you return, do the verification using Blink Comparison.
* Once your laptop is out of storage, check that the images you sent to yourself on Signal/Element aren't different from those on your GrapheneOS phone, and also check the timestamp. Once the verification is complete, you can delete the photos so that there is no confusion in future verifications about which photos to use. If the phone is found and the firmware or software is modified, Auditor will notify you. 

# Physical Intrusion Detection 

"Defense in depth" means that there are multiple layers of security that need to be bypassed for an adversary to succeed. [Physical intrusion detection](https://www.csrc.link/threat-library/mitigations/physical-intrusion-detection.html) should be done in addition to tamper-evident laptops and storage. This way, even if a covert house search doesn't interact with the tamper-evident storage (for example, because the goal is to install [covert surveillance devices](https://www.csrc.link/threat-library/techniques/covert-surveillance-devices.html)), you can still find out about it. 

Haven is an Android app that is developed by the Freedom of Press Foundation, which uses the smartphone’s many sensors — microphone, motion detector, light detector, and cameras — to monitor the room for changes, and it logs everything it notices. The version on [Github](https://github.com/guardianproject/haven) is more recent than the Google Play store, so [use Obtanium](/posts/grapheneos/#software-that-isn-t-on-the-play-store) to install it. Haven should be used on a dedicated cheap Android device that is otherwise empty - an older [Pixel](https://www.privacyguides.org/android/#google-pixel) is a good choice because it is cheap but has good cameras. Make sure that [Full Disk Encryption](/glossary/#full-disk-encryption-fde) is enabled. If you have a smartphone other than the dedicated Haven phone, it should be powered off in the tamper-evident storage - if Haven was running on it instead and was discovered by the intruder, they would now have physical access to it while it was turned on. 

* Place the Haven smartphone in a location that has a line of sight of where an intruder would need to pass through, such as a hallway that needs to be accessed to move between rooms or to access the storage. It should be plugged in so that the battery doesn't die; microUSB cables that are quite long can be obtained for this purpose. 
* Set a count down for it to turn on before you leave the house. The Haven app will log everything locally on the Android device. Remotely sending notifications is currently [broken](https://github.com/guardianproject/haven/issues/454).
* When you get home, check the Haven log. 

# Tamper-Evident Software and Firmware  

So far we have only looked at making hardware compromise tamper-evident. It is also possible to make software and firmware tamper-evident. "Defense in depth" requires this - to trust an electronic device, you need to trust the hardware, firmware, and software. Software or firmware compromise can happen [remotely](/glossary/#remote-attacks) (through the Internet) as well as with physical access, so it is especially important. Tamper-evident software and firmware is compatible with our [recommendations](/recommendations): Qubes OS or Tails on laptops, or GrapheneOS on a smartphone. 

For GrapheneOS, [Auditor](/posts/grapheneos/#auditor) is an app that will enable being notified if there is tampering with firmware or software -  you will receive an email when Auditor engages in remote attestation. For Tails or Qubes, [HEADS](https://osresearch.net/) can do the same before you enter your boot password (on [supported devices](https://osresearch.net/Prerequisites#supported-devices)). Keep the HEADS USB security dongle with you when you leave the house, and have a backup hidden at a trusted friend's house in case it ever falls into a puddle. 

# Wrapping Up 

With the measures described above, any 'evil maid' would need to bypass:
1) Haven detecting them, and
2) The tamper-evident storage, and
3) The tamper-evident glitter nail polish (for an attack that requires opening the laptop), or HEADS/Auditor (for a software or firmware attack)

These layers are all important, even if they may seem redundant. The expertise and expense of successfully executing the attack is increased substantially with every layer, which makes it much less likely that the attack will be attempted to begin with. It is best practice to [obtain a fresh device in a way that cannot be intercepted](/posts/tails-best/#reducing-risks-when-using-untrusted-computers) and then consistently implement all of these layers from the very beginning. 

That means that whenever you leave the house, you power off sensitive devices and put them into tamper-evident storage, take the required photos, and enable Haven. This might sound laborious, but it can be done in under a minute if you leave unused devices in storage. When you come home, first check the Haven log. Next, verify the tamper-evident storage. 

Laptop screws can be verified on a monthly basis, or if anything suspect happens. Neither HEADS nor Auditor require much effort after set-up to be used properly; Auditor will run without interaction, and HEADS becomes part of your booting process. 

# Further Reading
* [Random Mosaic – Detecting unauthorized physical access with beans, lentils and colored rice](https://dys2p.com/en/2021-12-tamper-evident-protection.html)

# Appendix: Cracking Safes

* [Rare-earth magnets](https://en.wikipedia.org/wiki/Safe-cracking#Magnet_risk) can unlock safes that use a [solenoid](https://www.youtube.com/watch?v=Y6cZrieFw-k) as the locking device, in a way that is not detectable.
* [Safe bouncing](https://en.wikipedia.org/wiki/Safe-cracking#Safe_bouncing) is when the locking mechanism can be moved sufficiently by [banging or bouncing the safe](https://mosandboo.com/how-to-open-a-safe-without-the-key-or-code/) to open it, in a way that is not detectable. Safes that use a gear mechanism are less susceptible to mechanical attacks. 
* A "management reset code" (also known as a "try-out combination") is present on many models of safes - if this code is not changed from its default then the safe can be unlocked in a way that is not detectable.  
* [Spiking](https://en.wikipedia.org/wiki/Safe-cracking#Spiking_the_lock) is when the wires on a cheap safe leading to the reset button, solenoid, or motor can be exposed, and spiked with a battery. This should be possible to make tamper-evident, as it requires getting access to the wires.
* [Brute force](/glossary#brute-force-attack) attacks - trying all possible combinations - are possible if the adversary has time. Dial mechanisms can be brute-forced with a [computerized autodialer](https://learn.sparkfun.com/tutorials/building-a-safe-cracking-robot) which [doesn't need supervision](https://www.youtube.com/watch?v=vkk-2QEUvuk). Electronic keypads are less susceptible to brute-forcing if they have a well-designed incremental lockout feature; for example, if you get it wrong 10 times, you're locked out for a few minutes, 5 more incorrect codes and you're locked out for an hour, etc. 
* Several tools exist that can automatically retrieve or reset the combination of an electronic lock; notably, the Little Black Box and Phoenix. Tools like these are often connected to wires in the lock that can be accessed without causing damage to the lock or container. This should be possible to make tamper-evident, as it requires getting access to the wires.
* Several [keypad-based attacks](https://en.wikipedia.org/wiki/Safe-cracking#Keypad-based_attacks) exist, but some can be mitigated with proper OPSEC. 
