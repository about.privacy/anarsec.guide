+++
title="GrapheneOS for Anarchists"
date=2023-04-05

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "mobile", "easy"]

[extra]
toc = true
blogimage="/images/graphene.avif"
+++

[Anarchists shouldn't have phones](/posts/nophones/). If you absolutely must use a phone, it should be as difficult as possible for an adversary to geotrack, intercept messages, or hack. This means using GrapheneOS. 
<!-- more -->
# What is GrapheneOS?
GrapheneOS is a private and secure version of the Android [operating system](/glossary#operating-system-os). Standard Android smartphones have Google baked into them (for example, [Google Play Services](https://en.wikipedia.org/wiki/Google_Play_Services) has irrevocable ability to access your files, call logs, location, etc.), and it is trivial to [bypass standard Android authentication](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance/authentication-bypass.html) with [physical access](/glossary/#physical-attacks) to the device. GrapheneOS uses hardware-based security to [substantially increase the difficulty](https://grapheneos.org/faq#encryption) of bypassing the authentication, it is significantly [hardened](/glossary#hardening) against hacking, and it has all Google apps and services removed by default. Other alternative Android operating systems exist [but they are inferior](https://blog.privacyguides.org/2022/04/21/grapheneos-or-calyxos/). See the [GrapheneOS documentation](https://grapheneos.org/features) for an extensive list of the privacy and security improvements over standard Android. GrapheneOS is [regularly audited](https://grapheneos.org/faq#audit).

Cellphones leave a geolocation history when they connect to cell towers, by nature of [how the technology works](https://anonymousplanet.org/guide.html#your-metadata-including-your-geo-location). For this reason, we recommend using a smartphone which stays at home like a landline, and connects to the Internet through Wi-Fi rather than by using a SIM card to connect through cell towers. Even if you use an anonymously acquired SIM, if this is linked to your identity in the future, the service provider can be retroactively queried for all geolocation data. Additionally, it's insufficient to only leave a phone at home when you are going to a demo or action because this then [stands out](/posts/nophones/#metadata-patterns) as an outlier, serving as an indication that there is conspiratorial activity in that time window. 

# Installation 
[Google Pixel](https://www.privacyguides.org/android/#google-pixel) phones are the only devices that currently meet GrapheneOS's hardware security requirements - see the [supported devices](https://grapheneos.org/faq#device-support) and [recommended devices](https://grapheneos.org/faq#recommended-devices). Beginning with the Pixel 6, Pixel devices receive a minimum of [5 years of security updates](https://grapheneos.org/faq#device-lifetime) from when they are released. End-of-life devices (GrapheneOS "extended support" devices) do not have full security updates so they are not recommended. Avoid carrier variants of the phone, meaning don't buy one from a mobile network operator, which can prevent you from installing GrapheneOS. The cheapest option is to buy the "a" model just after the next flagship model is released - for example, the Google Pixel 6a after the Pixel 7 is released. 

[Installing GrapheneOS](https://grapheneos.org/install/) can happen through a web browser, or through the [command line](/glossary#command-line-interface-cli). If you are uncomfortable with a command line, the web browser installer is fine; as the [instructions note](https://grapheneos.org/install/cli#verifying-installation), "Even if the computer you used to flash GrapheneOS was compromised and an attacker replaced GrapheneOS with their own malicious OS, it can be detected with Auditor", which is explained below. Both methods list their officially supported operating systems. There is not official support for installing from Qubes OS, but it is possible with the following steps.

<details>
<summary><strong>Installation on Qubes OS</strong></summary>
<p>TODO</p>
</details>

*These instructions assume that your sys-usb Qube is disposable, which is a [post-installation default](/posts/qubes/#getting-started).*

* In a Whonix-Workstation disposable qube, open the [command line installation guide](https://grapheneos.org/install/cli) using Tor Browser.  
* You will read "Installing from an OS in a virtual machine is not recommended. USB passthrough is often not reliable." This means that we will do everything from sys-usb, which does not use USB passthrough. If during the post-installation of Qubes OS you set sys-usb to be disposable, it will reset after a reboot. 
* For the sake of simplicity, we will enable networking in sys-usb temporarily. It is also possible to keep sys-usb offline by copying platform-tools and the factory image into sys-usb from a whonix disposable, and getting udev rules from [Github](https://github.com/M0Rf30/android-udev-rules) rather than apt. In the **Settings → Basic** tab of sys-usb, make the follow changes:
    * Private storage max size: 10.0 GB
    * Net qube: sys-firewall
    * Press **Apply**
* Follow the installation instructions from the sys-usb Terminal. When you get to **Flashing factory images**, don't run `./flash-all.sh`. Instead, scroll down to troubleshooting and run the command that uses a different temporary directory. It is expected for the flash script to output messages like `archive does not contain 'example.img'`. 
* When you are done, reboot sys-usb. If it is disposable, the changes you have made will be gone. Don't forget to change back the sys-usb qube settings:
	* Net qube: (none)

Upon first booting Graphene, it will prompt you if you want to connect to Wi-Fi. Don't, we need to do [hardware-based attestation](#auditor) first. Never set up the fingerprint authentication. Set a [strong password](/posts/tails-best/#passwords).  

# System navigation

By default, GrapheneOS uses [gesture navigation](https://grapheneos.org/usage#gesture-navigation). The essentials are:
* The bottom of the screen is a reserved touch zone for system navigation. 
* Swiping up from the navigation bar while removing your finger from the screen is the **Home** gesture.
* Swiping up from the navigation bar while holding your finger on the screen before releasing is the **Recent Apps** gesture.
* Swiping from either the left or the right of the screen within the app (not the navigation bar) is the **Back** gesture. 
* The launcher uses a swipe up gesture starting anywhere on the screen to open the app drawer from the home screen. You need to start that gesture above the system navigation bar.

# Auditor 

In the post-installation instructions, **Hardware-based attestation** is the last step. The Auditor app included in GrapheneOS leverages hardware security features in order to provide integrity monitoring to the device's firmware and software. This is crucial because it will alert you if the device is maliciously tampered with. The Auditor app must be configured directly after GrapheneOS is installed, before any Internet connection. 

How does it work? Your new device is the *auditee*, and the *auditor* can either be another instance of the Auditor app on a friend's phone or the [Remote Attestation Service](https://attestation.app/); we recommend doing both. The *auditor* and *auditee* pair to form a private key, and should tampering with the operating system of the *auditee* happen after the pairing is complete, the *auditor* will be alerted.

First, directly after the device has been installed and before connecting to the Internet, [perform local verification](https://attestation.app/tutorial#local-verification). This requires having a friend present who you see semi-regularly and who has the Auditor app (on any Android device). The first pairing will show a brown background, and subsequent audits will show attestation results with a green background if nothing is remiss. There is no remote connection between your phones; you need to re-audit to benefit. 

We recommend using the phone as a Wi-Fi only device. Turn on airplane mode, which will prevent your phone from being reached and tracked from the cellular network, and then enable Wi-Fi. Keep airplane mode turned on at all times - otherwise the phone will connect to mobile networks, even without a SIM card in it.  

You can now connect to Wi-Fi. Once you have an Internet connection, we recommend that you immediately set up the [scheduled remote verification](https://attestation.app/tutorial#scheduled-remote-verification) with an email that you check regularly. The default permitted delay until alerts is 48 hours; if you know your phone will be off for a longer amount of time, you can update the configuration to a maximum of two weeks. If your phone will be off for more than two weeks (for example, if you leave it at home during travel), just ignore the notification emails. You can log back in at any time to view the attestation history. 

# User Profiles 
User profiles are a feature that allows you to compartmentalize your phone, similar to how [Qubes OS](/posts/qubes/#what-is-qubes-os) will compartmentalize your computer. User profiles have their own instances of apps, app data, and profile data. Apps can't see the apps in other user profiles and can only communicate with apps within the same user profile. In other words, user profiles are isolated from one another - if one is compromised, the others aren't necessarily. 

The Owner user profile is the default profile which is there when you turn on the phone. Additional user profiles can be created. Each profile is [encrypted](/glossary/#encryption) using its own encryption key and cannot access the data of any other profiles. Even the device owner cannot view the data of other profiles without knowing their password. A shortcut to switch between different user profiles is present on the bottom of Quick Settings (accessible by swiping down from the top of the screen, twice). When you press **End session** on a profile, that profile's data is encrypted at rest.

We will now create a second user profile for all applications which don't require Google Play services:
* **Settings → System → Multiple users**, press **Add user**. You can name it Default, and press **Switch to Default**. 
* Set a [strong password](/posts/tails-best/#passwords) that is different from your Owner user profile password. 
* In the Default user profile, **Settings → Security → Screen lock settings → Lock after screen timout** can be set to 30 minutes to minimize how often you'll need to re-enter the password. 

Later on, we will optionally create a third user profile for applications that require Google Play services. 

To reiterate, the user profiles and their purposes will be:

**1) Owner**
* Where applications are installed 	

**2) Default**
* Where applications are used  

**3) Google (optional)**
* Where applications that require Google Play services are used

# How to Install Software
GrapheneOS's app store has the standalone applications built by the GrapheneOS project such as Vanadium, Auditor, Camera, and PDF Viewer. These are updated automatically. 

For installing additional software, avoid F-Droid due to its numerous [security issues](https://www.privacyguides.org/android/#f-droid). GrapheneOS has a [Sandboxed Google Play](https://grapheneos.org/features#sandboxed-google-play) which can be installed through the GrapheneOS app store: "Google Play receives absolutely no special access or privileges on GrapheneOS". 

The approach we will take is that all applications that are needed in any user profile will be installed into the Owner user profile, using Sandboxed Google Play. In the Owner user profile, all applications (except the VPN) will be disabled. The **Install available apps** feature will then be used to delegate apps to their needed profiles. Automatic updates in the Owner user profile will be applied in the secondary user profiles as well.  

To install and configure Sandboxed Google Play:
* Within the Owner user profile, install Sandboxed Google Play by opening Apps and install Google Play services (this will also install Google Services Framework and the Google Play Store).  
* The Google Play Store requires a Google account to log in, but one with false info can be created for exclusive use with the Google Play Store.   
* Once installed and logged in, disable the advertising ID: **Settings → Apps → Sandboxed Google Play → Google Settings → Ads**, and select *Delete advertising ID*. 
* Automatic updates are enabled in Google Play Store by default: **Google Play Store Settings → Network Preferences → Auto-update apps**.   
* Notifications for Google Play Store and Google Play Services need to be enabled for auto-updates to work: **Settings → Apps → Google Play Store / Google Play Services → Notifications**. If you get notifications from the Play Store that it wants to update itself, [accept them](https://discuss.grapheneos.org/d/4191-what-were-your-less-than-ideal-experiences-with-grapheneos/18).

You can now install applications through the Google Play Store. The first application we will install is a [VPN](/glossary/#vpn-virtual-private-network). If you will be using a free VPN, RiseupVPN is recommended. If you want to anonymously pay for a VPN, both [Mullvad](https://www.privacyguides.org/en/vpn/#mullvad) and [IVPN](https://www.privacyguides.org/en/vpn/#ivpn) are also recommended. VPNs are per-profile, so must be installed in each user profile separately. All default connections made by GrapheneOS will be forced through the VPN (other than [connectivity checks](https://grapheneos.org/faq#default-connections), which can optionally be disabled).

Using the example of RiseupVPN, once it is installed, accept the 'Connection request' prompt. A green display will mean that the VPN is successfully connected. Navigate to **Advanced settings** in the RiseupVPN menu, click **Always-on VPN**, and follow the instructions. Moving forward, the VPN will automatically connect when you turn on your phone. Continue to install any other apps - for ideas, see [Encrypted Messaging for Anarchists](/posts/e2ee/). 

Now we will delegate apps to their needed profiles:
* In the Owner profile, disable all applications other than the VPN: **Settings → Apps → [Example] → Disable**. 
* To install Riseup VPN (or any other app) in the Default user profile: **Settings → System → Multiple users → Default → Install available apps**, then select Riseup VPN. 

#### Software That Isn't On the Play Store
Some apps aren't on the Play Store, either because they are in development or they don't want users to have to interact with Google. The Play Store can be used to update apps, but when you download individual .apk files you will need to remember to update them yourself (there are exceptions, for example Signal is designed to self-update). [Obtainium](https://github.com/ImranR98/Obtainium) is an app to keep track of what apks need to be updated, and is available on the [GitHub Releases page](https://github.com/ImranR98/Obtainium/releases); `app-arm64-v8a-release.apk` of the latest release is what you want (arm64-v8a is the processor architecture). If you need apps that aren't on the Play Store, install Obtainium into the Owner user profile. Use the same process of installing .apk files into the Owner user profile, disabling them, and delegating apps to their needed profiles. 

As an example of how to use Obtainium, Molly-FOSS is a hardened version of Signal with [no Google software](https://github.com/mollyim/mollyim-android#free-and-open-source), and is available from [Github Releases](https://github.com/mollyim/mollyim-android/releases). In Obtanium press **Add App**, then copy the Github Releases URL. Obtanium can install the app, and when there is a new version you will get a system notification and an update icon will be present beside it, at which point you must manually update it.   


Cwtch is not yet present on the Google Play Store, and can be added to Obtainium by entering the [Download page URL](https://cwtch.im/download/).  

#### Software That Requires Google Play Services
If there is an app you would like to use that requires Google Play services, create a specific user profile for it from the Owner user profile; you can name it Google. This is also a good solution for isolating any app you need to use that isn't [open-source](/glossary/#open-source) or reputable. If you create a Google user profile, you will need to install and configure Sandboxed Google Play in it. 

Many [banking apps](https://grapheneos.org/usage#banking-apps) will require Sandboxed Google Play. However, banking can simply be accessed through a computer to avoid needing this Google user profile.  

# VoIP
A Wi-Fi only smartphone doesn't require paying a monthly fee for a SIM card. As explained in [Why Anarchists Shouldn't Have Phones](/posts/nophones#bureaucracy), bureaucracies often require a phone number that can be called normally (without encryption). [VoIP](/glossary#voip-voice-over-internet-protocol) applications allow you to create a number and make phone calls over the Internet rather than through cell towers. A phone number is also occasionally required for applications, such as [Signal registration](/posts/e2ee/#signal), and a VoIP number will often work. 

Some of the VoIP applications [that work on computers](/posts/nophones#bureaucracy) also work on smartphones; the main advantage is that you will hear it ring even when your computer is off. The [jmp.chat](https://jmp.chat/) VoIP service works well with their client [Cheogram](https://cheogram.com/) and can be paid for in Bitcoin. In addition, there are paid options that are only present on mobile such as MySudo (although it only works in a [handful of countries](https://support.mysudo.com/hc/en-us/articles/360020177133-Why-isn-t-MySudo-working-in-my-country-)). A MySudo subscription can be anonymously purchased with [Google Play gift cards](https://support.google.com/googleplay/answer/3422734), but this is likely unnecessary if the point is to give the number to bureaucracies. MySudo requires Google Play Services.  

# Tor 
Perhaps you want to use [Tor](/glossary/#tor-network) from a smartphone. However, if you need the anonymity of Tor rather than the privacy of Riseup VPN, you should use [either Qubes OS or Tails](/posts/qubes/#when-to-use-tails-vs-qubes-os) on a computer. The [Graphene docs](https://grapheneos.org/usage#web-browsing) recommend avoiding Gecko-based browsers like Tor Browser given that such browsers "do not have internal sandboxing on Android." Orbot is an app that can route traffic from any other app on your device through the Tor network, but simply using the Vanadium browser through Orbot is [not recommended by the Tor Project](https://support.torproject.org/tbb/tbb-9/). 

Applications like Cwtch and Briar have Tor built in, and should not be used through a VPN like Orbot. 

# Recommended Settings and Habits
* **Settings → Security → Auto reboot:** 8 hours [Owner user profile]
	* Auto reboot when no profile has been unlocked for several hours will put the device fully at rest again, where [Full Disk Encryption](/glossary/#full-disk-encryption-fde) is most effective. It will at minimum reboot overnight if you forget to turn it off. In the event of [malware](/glossary/#malware) compromise of the device, Verified Boot will prevent and revert changes to the operating system files upon rebooting the device. If police ever manage to obtain your phone when it is in a lock-screen state, this setting will return it to effective encryption even if they keep it powered on. 
* Keep the Global Toggles for Bluetooth, location services, the camera, and the microphone disabled when not in use. Apps cannot use disabled features (even if granted individual permission) until re-enabled.
	* **Settings → Connected devices → Bluetooth timeout:** 2 minutes 
* Quite a few applications allow you to "share" a file with them for media upload. For example, if you want to send a picture on Signal, do not grant Signal access to "photos and videos", because it will have access to all of your pictures then. Instead, in the Files app, long-press to select the picture, then share it with Signal.
* Once you have all the applications you need installed in a given user profile, disable app installation within it [Owner user profile].
	* **Settings → System → Multiple users → [Username]:** Disallow installing apps (enabled) 
* If an app asks for storage permissions, choose Storage Scopes. This makes the app assume that is has all of the storage permissions that were requested by it, despite not actually having any of them.
* It is convenient to be able to receive notifications from any user profile. Within the Owner user profile:
	* **Settings → System → Multiple users:** Send notifications to current user (enabled) 	

# How to Backup 
Don’t use cloud backups. You can't trust the corporate options, and they are the easiest avenue for police to access your data. If you need to backup your phone, backup onto your encrypted computer.

GrapheneOS currently provides Seedvault as a backup solution, but it's not very reliable. As the [docs specify](https://grapheneos.org/faq#file-transfer), connecting directly to a computer requires "needing to trust the computer with coarse-grained access", so it is best avoided. Instead, you can manually backup files by copying them to a USB-C flash drive with the Files app, or sending them to yourself via an encrypted messaging app like [Element (Matrix)](/posts/e2ee/#element-matrix). 

# Password Management
If you feel you need a password manager, [KeePassDX](https://www.privacyguides.org/en/passwords/#keepassdx-android) is a good option. However, most app credentials can be kept on KeePassXC on a computer as they don't need to be entered regularly. The set up described in this guide requires memorizing two passwords: 
1) The Owner user profile (boot password)
2) The Default user profile  
3) (Optional) Apps like [Cwtch](/posts/e2ee/#cwtch) and [Molly](/posts/e2ee/#signal) have their own passwords. 

Make sure to turn the phone off overnight and when you leave it at home. If police get access when the device is turned on (at a lock-screen), the "Auto reboot" setting will turn it off before they have much time to attempt a [brute-force](/glossary/#brute-force-attack). For advice on password quality, see [Tails Best Practices](/posts/tails-best/#passwords).

# Linux Desktop Phones

Why recommend a Pixel, and not a Linux desktop phone? Linux desktop phones such as the [PinePhone Pro](https://en.wikipedia.org/wiki/PinePhone_Pro) are [significantly easier to hack than GrapheneOS](https://madaidans-insecurities.github.io/linux-phones.html), as they do not have modern security features such as full system MAC policies, verified boot, strong app sandboxing, and modern [exploit](/glossary/#exploit) mitigations. Their hardware is architecturally lacking modern security features like hardware based encryption (via a TEE/Secure Element) and has questionable integration of components such as the modem. For this reason, we don't recommend Linux desktop phones.  

# Wrapping Up

With the set-up described, if a cop starts with your name, they won’t be able to simply look it up in a cellular provider database to get your phone number. If you use the phone as a Wi-Fi only device and always leave it at home, it cannot be used to determine your movement profile and history. If you use a VoIP number, it is accessed through a VPN so, even if this number is known, it can't be used to locate you. All communications with comrades use [end-to-end encryption](/posts/e2ee/); thus, they do not assist network mapping. Even if you have the bad fortune of being the target of a highly resourced investigation, the hardened operating system makes it more difficult to compromise with spyware, and such a compromise should be able to be [detected](#auditor). 

By storing the phone in a tamper-evident way when it is not in use, you'll be able to notice if it's been physically accessed. See the tutorial [Making Your Electronics Tamper-Evident](/posts/tamper/). 

The [forum](https://discuss.grapheneos.org/) is generally very helpful for any remaining questions you may have. 
