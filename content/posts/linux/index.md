+++
title="Linux Essentials"
description="The Basics Needed to Use Tails or Qubes"
date=2023-04-04

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "linux", "tails", "qubes", "easy"]

[extra]
blogimage="/gifs/destroy.gif"
toc=true
+++

As an anarchist, you've probably heard the recommendation to use a Linux computer. This article is intended to get you up to speed by giving a brief overview of what you need to know. 
<!-- more -->

# What is Linux, and Why Use It? 
If you are reading this, you probably use either Windows or macOS on your computer. These are both [operating systems](/glossary#operating-system-os), meaning the system software that runs your device. They are also both 'closed-source', meaning that the software '*source* code' is *closed* to the public, and so can't be inspected for privacy and security. Windows and macOS computers are sending your data to Microsoft and Apple, and you can't trust their [Full-Disk Encryption](/glossary#full-disk-encryption-fde) to protect your data if the computer is being [physically accessed](/glossary/#physical-attacks) (like after a [house raid](https://www.csrc.link/threat-library/techniques/house-raid.html)). 

Linux is a set of operating systems which are [open-source](/glossary#open-source), which means that the *source* code can be analyzed by anyone. Linux is the name for the core (**kernel**) of the operating system, and many different **distributions** (or 'distros') are based on it. Simply put, *Linux is the only type of computer that anarchists can put any trust in*. 

Linux distributions that anarchists are likely to have heard of are Debian, Ubuntu and Tails. Each different Linux distribution makes different choices about how to manage software, what kernel version to use, etc. In fact, both Ubuntu and Tails are adaptations of Debian for the specific use cases of being user-friendly (Ubuntu) and providing default anonymity (Tails).   

# How Software Works
In Linux, the term for an application is a **package**. Rather than downloading applications from various websites on the Internet (like in Windows and macOS), a Linux distribution will have a centralized **repository** where the software lives. This has the benefit that the integrity of the software is verified by the distribution, and it is guaranteed to work with that Linux distribution. It is still possible to install software from outside of a distro's repository, but it is generally considered to be riskier and verifying the integrity is your responsibility. Installing a package requires knowing its name, and all packages in a repository can be browsed through a web browser for [Debian](https://www.debian.org/distrib/packages#search_packages) as well as [Fedora](https://packages.fedoraproject.org/).

How do you actually install from the software repository? Each distribution also has a **package manager**, which is an application which installs software from a software repository. Debian, and distributions based on it, use the `apt` package manager. In some distributions, it is possible to install software with a Graphical User Interface (GUI) that is using the package manager in the background, like the [Synaptic Package Manager](https://tails.boum.org/doc/persistent_storage/additional_software/index.en.html#index3h1) in Tails. 

# Software Alternatives
Part of the learning curve to Linux is figuring out what open-source software to use, instead of the closed-source options you will be familiar with from Windows and macOS. For example, instead of using Microsoft Word, you can use LibreOffice. An application being open-source is an essential criteria, but is insufficient to be considered secure. For example, Telegram advertises itself as being open-source, but the servers are not open-source and the cryptography is [trash](https://anonymousplanet.org/guide.html#bad-cryptography). The list of [included software for Tails](https://tails.boum.org/doc/about/features/index.en.html#index1h1) will cover many of your needs with reputable choices. 

# The Command Line Interface
The dreaded [command line](/glossary/#command-line-interface-cli)! What even is it? You are used to interacting with applications through a **Graphical User Interface (GUI)**, which means through pointing and clicking buttons with your mouse. Some applications can also be interacted with through a **Command Line Interface (CLI)**, which is textual. Many applications will be available in both CLI and GUI versions. 

For example, navigating the contents of your computer with the File Manager GUI is pretty standard - you click on a folder (called a *directory* in Linux), and it opens. The same navigation around the file system is also possible from the CLI.

When you open a Terminal (the CLI application), there is a *prompt* waiting for you. It is called this because it is prompting you to say something, in a language that the Terminal understands. Prompts will differ in what information is displayed but all end with the `$` character. You then issue *commands* to the Terminal. The Terminal responds, then it redisplays the prompt in order to receive further commands.  

The best way to learn command line basics is to interact with it. We recommend this [Foundations: Linux Journey](https://techlearningcollective.com/foundations/linux-journey/the-shell) exercise to learn some basic commands. The [Software Distribution and Packages](https://techlearningcollective.com/foundations/linux-journey/software-distribution) exercise will teach you what you need to know to [install software in Qubes](/posts/qubes/#how-to-install-software).  

Some commands will require elevated permissions, equivalent to 'Open as Administrator' in Windows. For example, installing software typically requires this. Prepending `sudo` to a command will run it as the administrative user, named root (note: the root user is not the same as the root directory, and the two should not be confused). A root prompt will display `#` rather than `$`. Be especially careful with any command you run while using these elevated permissions, as you'll have the permissions necessary to wipe your entire disk or modify important files. It is helpful to know that text in the Terminal is pasted with Ctrl+Shift+V (i.e. the Shift key must also be pressed). 

Most Linux users will rarely need to use the CLI. For using [Tails](/tags/tails/), it shouldn't be required at all, although you will need the following commands for the [more secure installation](https://tails.boum.org/install/expert/index.en.html):
* `wget`: this downloads files from the Internet over the Command Line (rather than through a web browser)
* `gpg`: this handles [GPG encryption](/glossary#gnupg-openpgp) operations. It is how the integrity of the Tails download is verified. 
* `apt`: this manages packages on Debian. 
* `dd`: this copies a file from one disk to another. 

The [Qubes](/tags/qubes/) installation requires the same commands (during the [verification](https://www.qubes-os.org/security/verifying-signatures/) stage). The Command Line Interface is otherwise only required to install software:
* `apt install <PACKAGE_NAME>`: this installs packages on Debian 
* `dnf install <PACKAGE_NAME>`: this installs packages on Fedora 


If you ever don't understand what a command is meant to do, try searching [explainshell](https://explainshell.com/) for it.

#### GPG Explanation
Using `gpg` during the installation of Tails or Qubes OS will be less confusing if you understand how it works. 

First, some points of clarification. PGP and GPG are terms that can be used interchangeably; PGP (Pretty Good Privacy) is the encryption standard, and GPG (GNU Privacy Guard) is a program that implements it. PGP/GPG is also used for encrypted email communication ([though we don't recommend it](/posts/e2ee/#pgp-email)), but we are using it here exclusively to verify the integrity of files.  

GPG is a classic example of [public-key cryptography](/glossary/#public-key-cryptography). GPG provides cryptographic functions to [encrypt](/glossary/#encryption), decrypt, and sign files; our concern here is digitally signing files. The Qubes and Tails teams both generate a [digital signature](/glossary/#digital-signatures) on their .img releases. GPG gives us a way to verify that the file is truly 'signed' by the developers, which enables us to trust that it hasn't been tampered with.   

Now you need to understand the very basics of public-key cryptography. [This Computerphile video](https://invidious.sethforprivacy.com/watch?v=GSIDS_lvRv4&listen=false) has a great overview with visual aids. To summarize it, a **secret/private** key is used to **sign** messages, and only the user possessing this key can do so. Each **private** key has a corresponding **public** key - this is termed a **key pair**. The public key is shared with everyone, and this is used to verify the signature. Confused? Watch the video! 

Tails and Qubes OS sign their releases, and only they can do this because only they possess their private key. However, I can verify that this signature is valid by having a copy of their public key. Now let's walk through the [Tails verification instructions](https://tails.boum.org/install/expert/index.en.html), which is less convoluted than the [Qubes OS equivalent](https://www.qubes-os.org/security/verifying-signatures/). 

**Step: Create a Key-Pair**

Tails recommends this [Riseup guide](https://riseup.net/en/security/message-security/openpgp/gpg-keys#using-the-linux-command-line) to generate a key-pair. 
* `gpg --gen-key` will prompt you for some configuration options and then generate your key-pair.

**Step: Verify the Tails public key**
* `gpg --import < tails-signing.key` imports the Tails public key into your keyring, so that it can be used. 
* `gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export chris@chris-lamb.co.uk | gpg --import` imports a Debian developer's public key into your keyring, so that it can be used. 
* `gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F` allows you to verify the Tails public key with the Debian developer's public key, by examining the output as instructed. This is so that if the source of the Tails public key (tails.boum.org) is compromised, you have an external source of truth to alert you of this. 
* `gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F` certifies the Tails public key with the key you created in the last step. 

Now we know that we have a genuine version of the Tails public key.  `gpg` also knows this because we have decided to certify it. 

**Step: Verify your downloaded Tails .img file**

* `TZ=UTC gpg --no-options --keyid-format long --verify tails-amd64-5.10.img.sig tails-amd64-5.10.img` allows you to verify that the .img file is signed how it should be, by examining the output as instructed.  

Now we know that we have a genuine version of the Tails .img file, so can proceed to install it to a USB. 

# Going Further
If you'd like to learn more about Linux, we recommend:
* The rest of the Tech Learning Collective's [Foundations](https://techlearningcollective.com/foundations/) exercises will give you a much more comprehensive foundation than what you need to use Qubes or Tails.  
* [Linux Fundamentals on Hack The Box Academy](https://academy.hackthebox.com/course/preview/linux-fundamentals) is another interactive learning environment, with a less comprehensive overview.


