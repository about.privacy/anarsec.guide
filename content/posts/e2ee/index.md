+++
title="Encrypted Messaging for Anarchists"
date=2023-04-02

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "e2ee", "easy"]

[extra]
blogimage="/images/BASE_2.png"
toc=true
+++
Several different options are available for [end-to-end encrypted](/glossary/#end-to-end-encryption-e2ee) communications, with different trade-offs. This article will present an overview, as well as installation instructions for Tails, Qubes OS, and GrapheneOS. 
<!-- more -->
There are some concepts that need to be understood before going further, in order to distinguish the various options. 
* **End-to-end encryption** means that only you, and the person you communicate with, can read messages. However, not all [encryption](/glossary/#encryption) is created equal. The quality of the encryption is determined by the *encryption protocol* that is used, and how it is implemented at the software level. 
* **Metadata protection** means whether the [*metadata*](/glossary/#metadata) (the data about the data) about the communication is obscured. Even if the message itself is encrypted, metadata can reveal who is communicating with whom, when, how often, the sizes of whatever files may have been transferred, etc. Metadata exposure is [a major concern](https://docs.openprivacy.ca/cwtch-security-handbook/risk.html#threat-model). 
* **Peer-to-peer** means that there is no centralized server that you need to trust. 
* **Tor** is an [anonymity network](/glossary/#tor-network), and some applications route your messages through it by default. 

For a longer form look at these different considerations, we recommend [The Guide to Peer-to-Peer, Encryption, and Tor: New Communication Infrastructure for Anarchists](https://www.csrc.link/#the-guide-to-peer-to-peer-encryption-and-tor). This text criticizes Signal for not being peer-to-peer and not using Tor by default, and goes on to compare Signal, Cwtch, and Briar. The following options for encrypted messaging are listed from most metadata protection to least. 

<br>

<video controls="" width="99%">
   <source src="cwtch-explainer.mp4" type="video/mp4">
 </video>

# Cwtch
* **Mediums**: Text
* **Metadata protection**: Yes (strong)
* **Encryption protocol**: Tor Onion Services (v3) + [Tapir](https://docs.openprivacy.ca/cwtch-security-handbook/cwtch-overview.html)
* **Peer-to-peer**: Yes
* **Tor**: Yes

Cwtch is our preference, by a long shot. It is currently in transition from [beta to stable versions](https://docs.cwtch.im/blog/path-to-cwtch-stable). For an overview of how Cwtch works, see their [video explainer](https://cwtch.im/#how-it-works). The [Cwtch Handbook](https://docs.cwtch.im/) will tell you everything you need to know for using it. Cwtch is designed with metadata protection in mind; it is peer-to-peer, uses the Tor network as a shield and stores everything locally on-device, encrypted.

Like all peer-to-peer communication, Cwtch requires *synchronous* communication, meaning that both peers are online simultaneously. However, their server feature allows *asynchronous* communication as well by providing offline delivery:

>"Cwtch contact to contact chat is fully peer to peer, which means if one peer is offline, you cannot chat, and there is no mechanism for multiple people to chat. To support group chat (and offline delivery) we have created untrusted Cwtch [servers](https://docs.cwtch.im/docs/servers/introduction) which can host messages for a group. [...] the server has no way to know what messages for what groups it might be holding, or who is accessing it."

Any Cwtch user can turn the app on their phone or computer into an untrusted server to host a group chat, though this is best for temporary needs like an event or short-term coordination, because the device needs to stay powered on for it to work. Medium-term untrusted servers can be set up on a spare Android device that can stay on, and longer-term servers can be self-hosted on a VPS if you know Linux system administration. Once the server exists, contacts can be invited to use it. You can create a group chat with only two people, which enables asynchronous direct messages.  

>**Note**: [**Briar**](https://briarproject.org) is another application which works in a similar way (with peer-to-peer and Tor), and uses the [Bramble Transport Protocol](https://code.briarproject.org/briar/briar/-/wikis/A-Quick-Overview-of-the-Protocol-Stack) (BTP). The main distinguishing feature of Briar is that it continues to function [even when underlying network infrastructure is down](https://briarproject.org/how-it-works/). It was [audited in 2017](https://code.briarproject.org/briar/briar/-/wikis/FAQ#has-briar-been-independently-audited). Unfortunately, Briar Desktop does not yet work with Tails or Qubes-Whonix, because it cannot [use the system Tor](https://code.briarproject.org/briar/briar/-/issues/2095). Unlike Cwtch, to connect with a contact on Briar, you must both add each other first. You can either exchange `briar://` links or scan a contact’s QR code if they are nearby. [Briar Mailbox](https://briarproject.org/download-briar-mailbox/) enables asynchronous communication. 

<details>
<summary><strong>Cwtch Installation on GrapheneOS</strong></summary>
<br>
<p>If you have decided to use a smartphone despite our <a href="/posts/nophones/">recommendation to not use phones</a>, Cwtch is available for Android. Follow the instructions for <a href="/posts/grapheneos/#software-that-isn-t-on-the-play-store">installing software that isn't on the Play Store</a>. Updates must be made manually - back up your profile first.</p>
<br>
</details>

<details>
<summary><strong>Cwtch Installation on Tails</strong></summary>
<br>
<p>Cwtch is still beta - support for Tails is very new and not yet thoroughly tested.</p>
<ul>
<li>Start Tails with an Adminstration Password.</li>
<li>Download <a href="https://cwtch.im/download/#linux">Cwtch for Linux</a> using Tor Browser</li>
<li>Verify the download <ul>
<li>Open the folder from Tor Browser&#39;s download icon </li>
<li>Right click in the file manager and select &quot;Open a Terminal Here&quot;</li>
<li>Run <code>sha512sum cwtch-VERSION-NUMBER.tar.gz</code> (replacing the filename as appropriate)</li>
<li>Compare the hash of the file with what is listed on the download page </li>
</ul>
</li>
<li>As per our <a href="/posts/tails-best/#using-a-write-protect-switch">Tails Best Practices</a>, personal data should be stored on a second LUKS USB, not on the Tails Persistent Storage. Copy the file to such a personal data LUKS USB and extract it with the file manager (right click, select &quot;Extract Here&quot;). We will not be using the Additional Software Persistent Storage feature - Cwtch is an AppImage so doesn't require it. </li>
<li>Run the install script<ul>
<li>In the File Manager, enter to directory you just created, <code>cwtch</code>. Right click in the File Manager and select "Open a Terminal Here"</li>
<li>Run <code>install-tails.sh</code></li>
</ul>
</li>
<li>As the <a href="https://docs.cwtch.im/docs/platforms/tails">documentation</a> specifies, "When launching, Cwtch on Tails should be passed the CWTCH_TAILS=true environment variable". In the Terminal, run:<ul>
<li><code>exec env CWTCH_TAILS=true LD_LIBRARY_PATH=~/.local/lib/cwtch/:~/.local/lib/cwtch/Tor ~/.local/lib/cwtch/cwtch</code></li>
</ul>
</li>
<li>How you use Cwtch depends on whether you have enabled Persistent Storage: <ul>
<li>With Persistent Storage disabled, Cwtch must be re-installed every session you need to use it. Backup <code>`/home/amnesia/.cwtch/`</code> to the personal data LUKS USB, and copy it back into <code>/home/amnesia/</code> the next time you install Cwtch. </li>
<li>With Persistent Storage enabled and unlocked, in Terminal run <code>sudo sed -i '$ a /home/amnesia/.cwtch source=cwtch' /live/persistence/TailsData_unlocked/persistence.conf</code></li>
</ul>
</li>
<li>Updates must be made manually - back up your profile first.</li>
<br>
</details>

<details>
<summary><strong>Cwtch Installation on Qubes-Whonix</strong></summary>
<br>
<p>Cwtch on Whonix currently has an <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/550">issue</a> - support is forthcoming. </p>
</details>

![onionshare](onionshare.png)

# OnionShare 
* **Mediums**: Text
* **Metadata protection**: Yes (strong)
* **Encryption protocol**: Tor Onion Services (v3) 
* **Peer-to-peer**: Yes
* **Tor**: Yes

OnionShare has a [chat feature](https://docs.onionshare.org/2.6/en/features.html#chat-anonymously), which creates an ephemeral peer-to-peer chat room that is routed over the Tor network. Metadata protection works the same way as for Cwtch; it uses the Tor network as a shield and (ephemerally) stores everything locally on-device. The encryption protocol relies on Tor v3 onion service encryption; elliptic curve Diffie Hellman with Curve25519. Cwtch and Briar both have more features (including the additional Tapir and BTP encryption protocols), really the only benefit to OnionShare is that it is installed by default on Tails. 

<br>

![signal](signal.jpg) 

# Signal
* **Mediums**: Video call, voice call, text
* **Metadata protection**: Yes (Moderate)
* **Encryption protocol**: Signal Protocol, audited ([2017](https://en.wikipedia.org/wiki/Signal_Protocol))
* **Peer-to-peer**: No 
* **Tor**: Not default

The Signal Protocol has some metadata protection; [sealed sender](https://signal.org/blog/sealed-sender/), [private contact discovery](https://signal.org/blog/private-contact-discovery/), and the [private group system](https://signal.org/blog/signal-private-group-system/). Message recipient identifiers are only kept on the Signal servers as long as necessary in order to transmit each message. As a result, when Signal is served with a warrant, they [can only provide](https://signal.org/bigbrother/) the time of account creation and the date of the account's last connection to Signal servers, when provided with a phone number. Nonetheless, Signal is [reliant on the Google Services Framework](https://web.archive.org/web/20210728141938/https://serpentsec.1337.cx/signal-sucks-heres-why) (though it's possible to use without it) and the metadata protection of sealed sender [only applies to contacts (by default)](https://web.archive.org/web/20210728141938/https://serpentsec.1337.cx/signal-sucks-heres-why).  

Signal [is not peer-to-peer](https://www.csrc.link/#the-guide-to-peer-to-peer-encryption-and-tor); it operates centralized servers that we have to trust. Signal will work with Tor if it is used on an operating system that forces it; such as Whonix or Tails. 

However, registration for a Signal account is difficult to achieve anonymously. The account is tied to a phone number which the user needs to continue to control - due to [changes to "Registration Lock"](https://blog.privacyguides.org/2022/11/10/signal-number-registration-update/), it is no longer sufficient to register with a disposable phone number. In the future, Signal intends to make it so that accounts do [not require a phone number](https://signal.org/blog/building-faster-oram/), but until this is the case Signal cannot be easily used anonymously. An anonymous phone number can be obtained [on a burner phone or online](https://anonymousplanet.org/guide.html#getting-an-anonymous-phone-number), and then must be maintained. 

Another barrier to anonymous registration is that Signal Desktop only works if Signal is first registered from a smartphone. For users comfortable with the [command line](/glossary/#command-line-interface-cli), it is possible to register an account from a computer with [Signal-cli](https://0xacab.org/about.privacy/messengers-on-tails-os/-/wikis/HowTo#signal). The [VoIP](/glossary#voip-voice-over-internet-protocol) account used for the registration would need to be obtained anonymously. 

As a result, Signal is rarely used anonymously which has a significant impact if the State gets [physical](/glossary/#physical-attacks) or [remote](/glossary/#remote-attacks) access to the device. One of the primary goals of State surveillance of anarchists is [network mapping](https://www.csrc.link/threat-library/techniques/network-mapping.html), and it's not uncommon that they get physical access to devices through [house raids](https://www.csrc.link/threat-library/techniques/house-raid.html). For example, if device [authentication is bypassed](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance/authentication-bypass.html), it would then be possible to identify every Signal contact simply via their phone numbers (in addition to reading message history, etc.). 

Due to the near impossibility of using Signal anonymously as well as our [recommendation to not use phones](/posts/nophones/), we don't currently recommend anarchists use Signal. We nonetheless provide installation instructions because it has become the norm in the anarchist space in many countries, and it might be hard to get in touch with somebody without it.  

<details>
<summary><strong>Signal Installation on GrapheneOS</strong></summary>
</details>

If you have decided to use a smartphone [despite our recommendation to not use phones](/posts/nophones/), we recommend the [Signal Configuration and Hardening Guide](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/). As noted above, unless you are comfortable with the [Command Line Interface](/glossary/#command-line-interface-cli), Signal must be registered on a smartphone before being linked to a computer. Install Signal like you would for any [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software) (we don't recommend F-Droid). 

[Molly-FOSS](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/#molly-android) is a fork of Signal with hardening and anti-forensic features available on Android - we recommend it over Signal for anarchists, and extending trust to the Molly team is facilitated by its [reproducible builds](https://github.com/mollyim/mollyim-android/tree/main/reproducible-builds). Follow the instructions for [installing software that isn't on the Play Store](/posts/grapheneos/#software-that-isn-t-on-the-play-store). You can [migrate from an existing Signal account](https://github.com/mollyim/mollyim-android#compatibility-with-signal). Turn on database encryption. 

<details>
<summary><strong>Signal Installation on Tails</strong></summary>
</details>

About.Privacy [maintains a guide](https://0xacab.org/about.privacy/messengers-on-tails-os/) for installing Signal Desktop on Tails. There is a guide for registering an account from Tails without a smartphone (using Signal-cli), and another guide for if you already have a Signal account.  

Some of [Signal Configuration and Hardening Guide](https://blog.privacyguides.org/2022/07/07/signal-configuration-and-hardening/) also applies to Signal Desktop.

<details>
<summary><strong>Signal Installation on Qubes-Whonix</strong></summary>
</details>

Signal Desktop on Whonix is not guaranteed to have Tor Stream Isolation from other applications in the same qube, so we will install it in a dedicated qube. Signal Desktop is installed in a Template, not an App qube (because it is available as a .deb from a third party repository). 

* Go to **Applications menu > Qubes Tools > Qube Manager**
* Clone whonix-ws-16, and call it something like whonix-ws-16-signal. 
	* We do this to not add attack surface to the base Whonix Workstation template. If you also install other messaging applications like Element Desktop, they could share a cloned template with a name like whonix-ws-16-e2ee 
* Open a Terminal in the new Template: **Applications menu > Template: whonix-ws-16-signal: Xfce Terminal**
* Run the commands in the [Signal install guide](https://www.signal.org/download/linux/) to install Signal Desktop in the Template. 
	* Note that the layout of the Signal install guide is a bit confusing for users unfamiliar with the command line; `wget` and `cat` are separate commands, but `echo` in #2 is one command that is so long it takes two lines (which is why the second line is indented). 
	* Template qubes require a proxy for `wget`. Before running the command, create a configuration file at `~/.wgetrc` in the Template, with the contents:
```
use_proxy = on
http_proxy = 127.0.0.1:8082
https_proxy = 127.0.0.1:8082
```
* [Create an App qube](/posts/qubes/#how-to-organize-your-qubes) with the Template `whonix-ws-16-signal` and networking `sys-whonix`. 
* In the new App qube's **Settings > Applications** tab, bring Signal into the Selected column, and press **OK**.
* Updates will be handled by **Qubes Update** as you would expect. 
	
>**Alternative:** You can install Signal Desktop in a Whonix Workstation App qube by using [Qube Apps](https://micahflee.com/2021/11/introducing-qube-apps/), and you will not need to bother with Templates. Signal Desktop on Flathub is [community maintained](https://github.com/flathub/org.signal.Signal), not official, which [is a security consideration](https://www.kicksecure.com/wiki/Install_Software#Flathub_Package_Sources_Security). 

<br>
<br>

![element](element.png) 

# Element / Matrix
* **Mediums**: Video call, voice call, text
* **Metadata protection**: Poor
* **Encryption protocol**: vodozemac, audited ([2022](https://matrix.org/blog/2022/05/16/independent-public-audit-of-vodozemac-a-native-rust-reference-implementation-of-matrix-end-to-end-encryption))
* **Peer-to-peer**: No 
* **Tor**: Not default

Element is the name of the application (the client), and Matrix is the name of the network. A comparison to email may be helpful to understand it; Element is the equivalent of Thunderbird, whereas Matrix is the equivalent of the Simple Mail Transfer Protocol (SMTP) which underlies email. Element/Matrix is not peer-to-peer; you need to trust the server. However, unlike Signal, the servers are not centralized but rather federated - anyone can host their own. Unfortunately, the 'federation model' has the trade off that Matrix does [not have metadata protection](https://web.archive.org/web/https://serpentsec.1337.cx/matrix): "Federated networks are naturally more vulnerable to metadata leaks than peer-to-peer or centralized networks". To minimize this, see [Notes on the safe use of the Matrix service from Systemli](https://wiki.systemli.org/howto/matrix/privacy). 

Element will work with Tor if it is used on an operating system that forces it; such as Whonix or Tails. 

What homeserver you use is important— do not use the default homeserver matrix.org. [Systemli](https://www.systemli.org/en/service/matrix/) and [Anarchy Planet](https://anarchyplanet.org/chat.html) are reputable radical hosts. Systemli's instance has a default message retention time of [30 days](https://wiki.systemli.org/en/howto/matrix/max_lifetime), and IP addresses are not stored. 

Matrix can either be used through a web client (using Element Web on Tor Browser) or though a desktop client (using Element Desktop). The web clients for Systemli and Anarchy Planet are `element.systemli.org` and `anarchy.chat`, respectively. When using a desktop client, before trying to log in change the homeserver address to `https://matrix.systemli.org` or `https://riot.anarchyplanet.org`, respectively. It is easy to create an account anonymously, and does not require a phone. Systemli requires having an email account with them (which you need an invite to obtain), whereas anyone can sign up to Anarchy Planet with the registration code `aplanet`.  

A matrix ID looks like @username:homeserver, so for example, @anarsec:riot.anarchyplanet.org. Just like email, you can message accounts that are on different homeservers. 

As soon as you have logged in, go to **Settings > Security & Privacy**. 
* You will see that under **Where you're signed in** it lists all signed-in devices. For anonymous use cases, you will generally only be signed-in on one device. 
* Scroll down to **Secure Backup**. This is a feature that allows you to verify a new session without having access to a signed-in device. Press **Set up**, then the **Generate a Security Key** choice. Save the Security Key in KeePassXC. This "Security Key" will be needed for logging into a new device or session. 
	* For Element Desktop, you will only need to use the Security Key if you sign out. 
	* For Element Web (using Tor Browser), you will need the Security Key every time you use it. Tor Browser clears your cookies, so you will need to sign in to a new session.  

Some current limitations:
* "Disappearing messages" is not yet a feature, but it is forthcoming. Message retention time can be set by the homeserver administrator, as mentioned above, and it is indeed set on both of our recommended homeservers. 
* One to one audio/video calls [are encrypted](https://matrix.org/faq/#are-voip-calls-encrypted%3F) and you can use them. Group audio/video calls are not encrypted, so don't use them. This will be resolved when [Element-call](https://github.com/vector-im/element-call) is stable. 
* The Matrix protocol itself [theoretically](/glossary#forward-secrecy) supports [Forward Secrecy](/glossary#forward-secrecy), however this is [not currently supported in Element](https://github.com/vector-im/element-meta/issues/1296) due to it breaking some aspects of the user experience such as key backups and shared message history. 
* Profile pictures, reactions, and nicknames are not encrypted.

>**Note**: You may have heard of **XMPP** (formerly called Jabber). XMPP has similar security properties to Matrix, but many clients don't support end-to-end encryption (via the OMEMO protocol) by default. Configuring a client properly is non-trivial. XMPP and Matrix leak similar amounts of metadata, but OMEMO has never been formally audited like the Matrix encryption protocol. Additionally, the administrator is able to act as a [man-in-the-middle](/glossary#man-in-the-middle-attack) on [any XMPP server](https://web.archive.org/web/20211215132539/https://infosec-handbook.eu/articles/xmpp-aitm/). For these reasons, we recommend using Matrix over XMPP. 

<details>
<summary><strong>Element Installation on GrapheneOS</strong></summary>
</details>

If you have decided to use a smartphone despite our [recommendation to not use phones](/posts/nophones/), Element is available for Android. Install Element like you would for any [app that doesn't require Google Services](/posts/grapheneos/#how-to-install-software) (we don't recommend F-Droid).  

<details>
<summary><strong>Element Installation on Tails</strong></summary>
</details>

The easiest option is to use the Element web client on Tor Browser. This doesn't require any additional software. Tor Browser deletes all data upon closing, so you'll be prompted for the Security Key after each login in order to access your past messages. Make sure to **Sign Out** when finished, to avoid accumulating "Signed-in devices".

To install Element Desktop, About.Privacy [maintains a guide](https://0xacab.org/about.privacy/messengers-on-tails-os/). 

<details>
<summary><strong>Element Installation on Qubes-Whonix</strong></summary>
</details>

The easiest option is to use the Element web client on Tor Browser is a disposable Whonix qube. This doesn't require any additional software. Tor Browser deletes all data upon closing, so you'll be prompted for the Security Key after each login in order to access your past messages. Make sure to **Sign Out** when finished, to avoid accumulating "Signed-in devices". 

To install Element Desktop, Whonix is not guaranteed to have Tor Stream Isolation from other applications in the same qube, so we will install it in a dedicated qube. Element Desktop is installed in a Template, not an App qube (because it is available as a .deb from a third party repository). 

* Go to **Applications menu > Qubes Tools > Qube Manager**
* Clone whonix-ws-16, and call it something like whonix-ws-16-element. 
	* We do this to not add attack surface to the base Whonix Workstation template. If you also install other messaging applications like Signal Desktop, they could share a cloned template with a name like whonix-ws-16-e2ee 
* Open a Terminal in the new Template: **Applications menu > Template: whonix-ws-16-element: Xfce Terminal**
* Run the commands in the [Element install guide](https://element.io/download#linux) to install Element Desktop in the Template. 
	* Template qubes require a proxy for `wget`. Before running the command, create a configuration file at `~/.wgetrc` in the Template, with the contents:
```
use_proxy = on
http_proxy = 127.0.0.1:8082
https_proxy = 127.0.0.1:8082
```
* [Create an App qube](/posts/qubes/#how-to-organize-your-qubes) with the Template `whonix-ws-16-element` and networking `sys-whonix`. 
* In the new App qube's **Settings > Applications** tab, bring Element Desktop into the Selected column, and press **OK**.
* Updates will be handled by **Qubes Update** as you would expect. 
* Avoid pressing "Sign Out", simply shutdown the qube when finished. 
	
>**Alternative:** You can install Element Desktop in a Whonix Workstation App qube by using [Qube Apps](https://micahflee.com/2021/11/introducing-qube-apps/), and you will not need to bother with Templates. Element Desktop on Flathub is [community maintained](https://github.com/flathub/im.riot.Riot), not official, which [is a security consideration](https://www.kicksecure.com/wiki/Install_Software#Flathub_Package_Sources_Security). 

<br>
<br>

![pgp](pgp.webp)

# PGP Email
* **Mediums**: Text
* **Metadata protection**: No
* **Encryption protocol**: [RSA](https://blog.trailofbits.com/2019/07/08/fuck-rsa/) or ed25519, no forward secrecy
* **Peer-to-peer**: No 
* **Tor**: Depends

PGP (Pretty Good Privacy) isn't so much a messaging platform as it is a way of encrypting messages on top of existing messaging platforms (in this case, email). PGP email is the only option presented which does not have the encryption property of [*forward secrecy*](/glossary/#forward-secrecy). The goal of forward secrecy is to protect past sessions against future compromises of keys or passwords. It maintains the secrecy of past communications even if the current one is compromised. This means that an adversary could decrypt all PGP messages in the future in one fell swoop. Once you also take into account the metadata exposure inherent in email, PGP should be disqualified from inclusion in this list. It simply doesn't meet the standards of a modern cryptography. However, given that it is already widely used within the anarchist space, we include it here as a warning that it is not recommended. For a more technical criticism, see [The PGP Problem](https://latacora.micro.blog/2019/07/16/the-pgp-problem.html) and [Stop Using Encrypted Email](https://latacora.micro.blog/2020/02/19/stop-using-encrypted.html). We recommend switching to Element for asynchronous use cases, and switching to Cwtch for synchronous use cases. If you need to use email, use a [radical server](https://riseup.net/en/security/resources/radical-servers) and see the [Riseup Guide to Encrypted Email](https://riseup.net/en/security/message-security/openpgp). 

PGP is used for another purpose outside of communication: to verify the integrity of files. For this use, see our [GPG explanation](/posts/linux/#gpg-explanation).


