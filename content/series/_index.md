+++
title = "Series"
description = ""
sort_by = "date"
paginate_by = 5
+++
<br>

# Defensive

#### Tails
* [Tails for Anarchists](/posts/tails/)
* [Tails Best Practices](/posts/tails-best/)

#### Qubes OS
* [Qubes OS for Anarchists](/posts/qubes/)

#### Phones 
* [Why Anarchists Shouldn't Have Phones](/posts/nophones/)
* [GrapheneOS for Anarchists](/posts/grapheneos/)

#### Tails and Qubes OS
* [Linux Essentials: The Basics Needed to Use Tails or Qubes](/posts/linux/)
* [Removing Identifying Metadata From Files](/posts/metadata/)

#### Tails, Qubes OS, and GrapheneOS 
* [Encrypted Messaging for Anarchists](/posts/e2ee/)
* [Making Your Electronics Tamper-Evident](/posts/tamper/)
