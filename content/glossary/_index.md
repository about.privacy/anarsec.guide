+++
title = "Glossary"
sort_by = "date"
paginate_by = 5
+++
<br>

> This glossary defines terms often used in AnarSec articles. 

### Asynchronous Communication

Both parties do not need to be online simultaneously, in contrast to [synchronous communication](#synchronous-communication). This relies on a server of some kind that will store messages until message recipients come online. This is the type of messaging that most people are familiar with (email, Signal, etc.).   

### Backdoor

A backdoor in software or hardware allows an unauthorized party to bypass access control. For instance, an undocumented developer account in a router will enable developers of this product to bypass the login form. Third parties can also use backdoors to access software/hardware. Hackers want to establish backdoors, and so do police agencies. 

### Brute-force attack

An attacker who “simply” tries every possible key to access a service or decrypt a file uses “brute force.” This process is called a brute-force attack. More efficient computers make brute-force attacks more feasible. Modern cryptographic protocols are designed to force an adversary (not in possession of the cryptographic key) to spend (close to) as much time as it would take to try every possible key to break the code. The parameters of a good protocol are chosen so that this amount of time is impractical. 

### Checksums / Fingerprints
Checksums are digital fingerprints: small-sized blocks of data derived from another block of digital data for the purpose of detecting any changes that may have been introduced. For example, when you download an operating system .iso file, a checksum will be listed that looks like: `sha512: 9f923361887ac4b1455bc5ae51c06f2457c6d(continued...)`. You can use [hash functions](https://open.oregonstate.education/defenddissent/chapter/cryptographic-hash/) like sha512 to create fingerprints. Essentially, this mathematical operation converts the 0s and 1s of the file into a unique "fingerprint". If a single 1 or 0 is changed, a completely different fingerprint will result, and it is often important to know whether a file has changed, such as when downloading the image file for an operating system. Fingerprints are often used in cryptography (e.g., in certificates or to verify [public keys](#public-key-cryptography) in general). [GtkHash](https://tails.boum.org/doc/encryption_and_privacy/checksums/index.en.html) is a program that allows you to calculate checksums without needing to use a command line interface.  

### Command Line Interface (CLI)
The 'command line' is an all-text alternative to using the graphical 'point and click' tool that most of us are more familiar with; the Command Line Interface (CLI) allows us to do some things that a Graphical User Interface (GUI) does not. Oftentimes, either a GUI or CLI would work and which you use is a matter of preference. For example, in [Tails](#tails), you can verify the [checksum](/glossary/#checksums-fingerprints) of a file with a GUI (the GtkHash program) or a CLI command (`sha256sum`). 

[Tech Learning Collective's "Foundations: Command Line Basics" course](https://techlearningcollective.com/foundations/#foundations-command-line-basics) is our recommended introduction to CLI/terminal use.

### CVE

CVE stands for “Common Vulnerabilities and Exposures”. It is a globally unique identifier for [security vulnerabilities](#vulnerability) in software. Identifiers look like “CVE-YEAR-NUMBER.” The year included in the identifier is the year when the CVE ID was assigned, not the year when the vulnerability became publicly known.

### DDoS Attack
A DDoS attack (Distributed Denial of Service attack) tries to overload or crash services on the targeted system by sending a large number of requests from numerous sources. The goal of DDoS attacks is to affect the availability of a service or system, e.g., making a web server unreachable for web browsers.

### Digital Signatures
Digital signatures are based on [public-key cryptography](#public-key-cryptography). A private key is used to digitally sign data, while the corresponding public key is used by third parties to validate the signature. Before a public key is used to verify a signature, its authenticity should be verified.

To learn more, [check out this video](https://invidious.sethforprivacy.com/watch?v=s22eJ1eVLTU&listen=false). For a more detailed look, see [Defend Dissent: Authenticity through Cryptographic Signing](https://open.oregonstate.education/defenddissent/chapter/cryptographic-signing/) or our [GPG explanation](/posts/linux/#gpg-explanation).

### Doxxing

Publicly releasing private data about an individual or organization is called doxxing. Before publication, the person conducting doxing can use public databases, social media, or [social engineering](/glossary/#social-engineering) to acquire information.

### Encryption

Encryption is the process of scrambling a message so that it can only be unscrambled (and read) by the intended parties. The method by which you scramble the original message, or *plaintext*, is called the *cipher* or *encryption protocol*. In almost all cases, the cipher is not intended to be kept secret. The scrambled, unreadable, encrypted message is called the ciphertext and can be safely shared. Most ciphers require an additional piece of information called a *cryptographic key* to encrypt and decrypt (scramble and unscramble) messages.

For more info, see [symmetric cryptography](/glossary/#symmetric-cryptography), [asymmetric cryptograph](/glossary/#public-key-cryptography), or [Defend Dissent: What is Encryption?](https://open.oregonstate.education/defenddissent/chapter/what-is-encryption/)

### End-to-end encryption (e2ee)

The data is [encrypted](#encryption) while in transit from one device to another - ‘endpoint’ to ‘endpoint’, and cannot be decrypted by any intermediaries. It can only be decrypted by the endpoints. This is distinct from ‘encryption at rest’, like in [Full Disk Encryption](#full-disk-encryption-fde), where the data stored on your device is encrypted once the device is turned off. Both are important! 

For more info, check out [Encrypted Messaging for Anarchists](/posts/e2ee), and [Defend Dissent: Protecting Your Communications](https://open.oregonstate.education/defenddissent/chapter/protecting-your-communications/).

### Exploit

An exploit is designed to exploit a [vulnerability](#vulnerability). Even worse (or better, depending on if you are the attacker or attacked) are [zero-day exploits](#zero-day-exploit).

### Forward secrecy

Forward secrecy (FS, also known as “Perfect Forward Secrecy”) combines a system of long-term keys and session keys to protect encrypted communications against key compromise in the future. An attacker who can record every encrypted message ([man-in-the-middle](#man-in-the-middle-attack)) won’t be able to decrypt these messages when keys are compromised in the future. Modern encryption protocols like [TLS](#https) 1.3 and the Signal Protocol offer FS. To learn more, see [Anonymous Planet](https://anonymousplanet.org/guide.html#forward-secrecy). 

### Full Disk Encryption (FDE)

FDE means that the entire storage medium is [encrypted](#encryption) until a password is provided when the device is turned on. Not all FDE is created equal. For example, the quality of how FDE is implemented on a phone isn’t only tied to your operating system, but also to your hardware (the model of your phone). FDE uses [symmetric cryptography](/glossary/#symmetric-cryptography), and, on Linux, it typically uses the [LUKS specification](/glossary/#luks). 

### GnuPG / OpenPGP

GnuPG (GPG) is a program that implements the OpenPGP (Pretty Good Privacy) standard. GPG provides cryptographic functions to encrypt, decrypt, and sign text and files. It is a classic example of [public-key cryptography](#public-key-cryptography). When used in tandem with email, [metadata](#metadata) (like email addresses) remains unencrypted. It does not provide [Forward Secrecy](#forward-secrecy).

For more info, check out [this primer](https://github.com/AnarchoTechNYC/meta/wiki/Pretty-Good-Privacy-%28PGP%29). We do not recommend it for encrypted communications, [here's why](/posts/e2ee/#pgp-email). 

### Hardening

Hardening is a generic term for the process of securing systems against attacks. 

### HTTPS

The 'S' in HTTPS stands for 'secure'; this means your Internet connection is encrypted using the [Transport Layer Security (TLS)](https://invidious.sethforprivacy.com/watch?v=0TLDTodL7Lc&listen=false) protocol. This involves the website generating a certificate with [public-key cryptography](#public-key-cryptography) that can be used to verify its authenticity - that you are in fact connecting to the web server that you intend to, and that this connection is encrypted.   

For more info, check out [our explanation](/posts/tails/#tor) or [Defend Dissent: Protecting Your Communications](https://open.oregonstate.education/defenddissent/chapter/protecting-your-communications/).

### Linux

Linux is an [open-source](#open-source) 'kernel' upon which operating systems are built. Unlike Windows or macOS, there are many flavours of Linux operating systems. For example, Ubuntu, Kali, and Tails are based on Debian. Manjaro is based on Arch. For more info, see [Linux Essentials](/posts/linux).

### LUKS 

The [Linux Unified Key Setup (LUKS)](https://gitlab.com/cryptsetup/cryptsetup) is a platform-independent specification for hard disk encryption. It is the standard used in [Tails](#tails), [Qubes OS](#qubes-os), Ubuntu, etc. 

### Malware

Malware (malicious software) is a generic term for software containing unwanted or malicious functions. Malware includes ransomware, Trojan horses, computer viruses, worms, spyware, scareware, adware, etc. Nowadays, malware is harder to categorize because sophisticated malware often combines properties of different categories. For instance, [WannaCry](https://en.wikipedia.org/wiki/WannaCry_ransomware_attack) propagated like a worm but encrypted files and demanded ransom (ransomware).

### Man-in-the-middle attack

An example of a man-in-the-middle attack is when Alice communicates with Bob via the Internet, Eve (Eavesdropper) joins the conversation “in the middle” and becomes man-in-the-middle. Eve can modify, insert, replay, or read messages at will. Protective measures are encryption (confidentiality) and checking the authenticity and integrity of all messages. However, one must also ensure that one is communicating with the expected party. You have to verify that you possess the real public key of the respective recipient. For instance, this is what you are doing when verifying the 'Safety Number' of a contact in the Signal encrypted messaging app.

For a more detailed look, check out [Defend Dissent: The Man in the Middle](https://open.oregonstate.education/defenddissent/chapter/the-man-in-the-middle/)

### Metadata

Metadata is data that provides information about other data. For instance, a JPG file contains the actual picture (data) but also may contain metadata like the date when the file was made, type of camera, GPS coordinates, etc. Metadata can be valuable for attackers (finding appropriate exploits for outdated software used by the victim), government agencies (collecting information about people to create social graphs), and other parties (who aim to show location-based advertisements). As soon as you use any computer, you are likely leaving metadata behind.

For more info, check out [Removing Identifying Metadata From Files](/posts/metadata) and [Defend Dissent: Metadata](https://open.oregonstate.education/defenddissent/chapter/metadata/).

### Open-source

The only software we can trust because the ‘source code’ that it is written in is ‘open’ for anyone to examine.

### Operating system (OS)

The system software that runs your device, before all other software. Some common examples are Windows, macOS, Linux, Android, and iOS. Linux and some versions of Android are the only open-source options in this list. 

### Passphrase

A passphrase is similar to a [password](#password); however, it consists of words instead of characters. 

### Password

A password is a string of characters used for authentication. A strong password consists of randomly chosen characters that all have an identical probability of occurrence, which can be created with the KeePassXC Password Generator. 

For more info, check out [Defend Dissent: Passwords](https://open.oregonstate.education/defenddissent/chapter/passwords/)

### Phishing

Phishing is a [social engineering](/glossary/#social-engineering) technique. Attackers send SMS messages, emails, chat messages, etc., to their victims in order to get their personal data. After that, attackers can try to impersonate their victims. It can also be used to make the victim download [malware](#malware) onto a system, which can be used as a starting point for hacking it. [Spear phishing](/glossary/#spear-phishing) is a more sophisticated phishing technique.

### Physical attacks

By a physical attack, we mean a situation in which an adversary first gains physical access to your device through loss, theft, or confiscation. For example, your phone may be confiscated while crossing a border or during an arrest. This is in contrast to a [remote attack](#remote-attacks). 

For a more detailed look, check out [Defend Dissent: Protecting Your Devices](https://open.oregonstate.education/defenddissent/chapter/protecting-your-devices/)

### Plausible deniability

Plausible deniability can be a security goal. It is accomplished if you can’t prove that a person/system sent a particular message. Then, this person/system can plausibly deny being the sender of the message.

### Public-key cryptography

Public-key cryptography (or asymmetric cryptography) is the opposite of [symmetric cryptography](/glossary/#symmetric-cryptography). Every party has two keys (public and private). The private one must be kept secret and is used for decryption; the public one has to be published, and it is used for encryption. This is the model used for encrypted communications, because the public key (again, required to be in the open) cannot be used to decrypt. All other parties must verify that a published public key belongs to the anticipated owner to avoid [man-in-the-middle attacks](/glossary/#man-in-the-middle-attack).

There are different approaches to public-key cryptography. For example, some cryptosystems are based on the algebraic structure of elliptic curves over finite fields (ECC). Others are based on the difficulty of the factorization of the product of two large prime numbers (RSA). Public-key cryptography can also be used for [digital signatures](/glossary/#digital-signatures). 

To learn more, check out [this video](https://invidious.sethforprivacy.com/watch?v=GSIDS_lvRv4), or for a more detailed look, check out [Defend Dissent: Public-Key Cryptography](https://open.oregonstate.education/defenddissent/chapter/public-key-cryptography/).

### Qubes OS

You can think of [Qubes OS](https://www.qubes-os.org/) as Linux + [virtual machines](#virtual-machine-vm). We [recommend](/recommendations) it as a daily use operating system for intermediate Linux users. 

### Remote attacks

By a remote attack, we mean that an adversary would access the data on your phone or laptop through an Internet or data connection. There are companies that design and sell the ability to infect your device (usually focusing on smartphones) with [malware](#malware) that would allow their customer (your adversary, be it a corporate or state agent) to gain remote access to some or all your information. This is in contrast to a [physical attack](#physical-attacks).

For a more detailed look, check out [Defend Dissent: Protecting Your Devices](https://open.oregonstate.education/defenddissent/chapter/protecting-your-devices/)

### Sandboxing

Sandboxing is software-based isolation of applications to mitigate system failures or vulnerabilities. For example, if an attacker hacks an application that is 'sandboxed', they will need to escape the sandbox to hack the whole system. [Virtualization](#virtualization) is the most powerful implementation of sandboxing. 

### Security goal

Security goals are concepts in information security that define what has to be fulfilled. Well-known security goals are confidentiality, integrity, and availability (referred to as the CIA triad).

### Social engineering

Social engineering is a generic term for the psychological manipulation of humans into performing actions. Social engineering isn’t dependent on technology; it is quite common in everyday life. For example, children cry to manipulate their parents; commercials manipulate their viewers. In information security, [phishing](/glossary/#phishing) is a widespread social engineering technique.

### Spear phishing

Spear phishing is more sophisticated than regular [phishing](/glossary/#phishing) which casts a wide net. In spear phishing, attackers customize their forged messages and send them to a smaller amount of potential victims. Spear phishing requires more research on the attacker’s side; however, the success rate of spear phishing attacks is higher than the success rate of regular phishing attacks.

### Supply-chain attack

A supply-chain attack can affect any user of hardware or software components. Attackers manipulate a component during its manufacturing process. In most cases, the actual attack happens before the targeted user possesses the manipulated component. Examples are manipulated compilers or firmware, and attacks like [Stuxnet](https://en.wikipedia.org/wiki/Stuxnet) or [SolarWinds](https://en.wikipedia.org/wiki/2020_SolarWinds_watering_hole_attack).

### Symmetric cryptography

Symmetric cryptography is the opposite of [public-key cryptography](/glossary/#public-key-cryptography). Two parties need the same private key to communicate. Both of them use this key for encryption and decryption. Symmetric encryption is faster than public-key encryption; however, you have to exchange keys securely. AES is a well-known representative of symmetric cryptography. This is the model used for [Full Disk Encryption](/glossary/#full-disk-encryption-fde) (for example, used by [LUKS](/glossary/#luks) in Linux Full Disk Encryption). 

### Synchronous communication

Both parties need to be online simultaneously, in contrast to [asynchronous communication](#asynchronous-communication). This does not require any servers specific to the communication and is often referred to as "peer to peer". 

### Tails
Tails is an operating system that makes secure and anonymous computer use accessible to anyone. Tails runs from a USB drive, and is designed to leave no trace of your activity on your computer, unless you explicitly ask it to.

Tails relies on the [Tor anonymity network](/glossary/#tor-network) to protect your privacy online; all software is configured to connect to the Internet through Tor. If an application tries to connect to the Internet directly, the connection is automatically blocked for security.

For more info, check out [Tails for Anarchists](/posts/tails).

### Threat model

Threat modeling is a family of activities for improving security by identifying a range of adversaries, [security goals](#security-goal), and [vulnerabilities](#vulnerability), then defining countermeasures to prevent, or mitigate the effects of, threats to the system. A threat is a potential or actual undesirable event that may be malicious (such as [DDoS attack](#ddos-attack)) or incidental (such as a hard drive failure). Threat modeling is the conscious activity of identifying and assessing threats and vulnerabilities.

For more info, see [the CSRC Threat Library](https://www.csrc.link/threat-library/), [Defend Dissent: Digital Threats to Social Movements](https://open.oregonstate.education/defenddissent/chapter/digital-threats/) and [Defending against Surveillance and Suppression](https://open.oregonstate.education/defenddissent/chapter/surveillance-and-suppression/).

### Tor network  

[Tor](https://www.torproject.org/) (it stands for The Onion Router) is an open and distributed network that helps defend against traffic analysis. Tor protects you by bouncing your communications around a network of relays run by volunteers all around the world: it prevents somebody watching your Internet connection from learning what sites you visit, and it prevents the operators of the sites you visit from learning your physical location.

Each website visited through the Tor network passes through 3 relays. Relays are servers operated by different people and organizations around the world. A single relay never knows both where the encrypted connection is coming from and where it is going to. An extract of a leaked Top Secret appraisal by the NSA characterized Tor as "the King of high secure, low latency Internet anonymity" with "no contenders for the throne in waiting". The Tor network can be accessed through the Tor Browser on any operating system. The operating system [Tails](#tails) forces every program to use the Tor network when accessing the Internet. 

For more info, see [our description of Tor](/posts/tails/#tor).

### Two-Factor Authentication (2FA)

Two-factor authentication (or “2FA”) is a way to let a user identify themselves to a service provider by requiring a combination of two different authentication methods. These may be something that the user knows (like a password or PIN) or something that the user possesses (like a hardware token or mobile phone). 

### Virtualization

Virtualization is technology that creates a virtual version of something, including virtual computer hardware platforms, storage devices, and computer network resources. A [Virtual Machine](#virtual-machine-vm) leverages this technology.

### Virtual Machine (VM)

A virtual machine is the [virtualization](#virtualization)/emulation of a computer system. Virtual machines are based on computer architectures and provide the functionality of a physical computer. This can provide the security benefit of [sandboxing](#sandboxing). [Qubes OS](#qubes-os) is composed of VMs that [run on the hardware directly](https://www.qubes-os.org/faq/#how-does-qubes-os-compare-to-running-vms-in-a-conventional-os) (which is referred to as "bare metal"). According to the Qubes project, "virtualization is currently the only practically viable approach to implementing strong isolation while simultaneously providing compatibility with existing applications and drivers."

### VoIP  (Voice over Internet Protocol)

Google Voice is a well-known and insecure VoIP service; this technology puts your calls through the Internet (like how Signal does) instead of using the standard cell tower transmission. Unlike Signal, with VoIP you can receive calls from anyone, not just other Signal users. The benefit of using VoIP for calls compared to a data plan is that you can generate different numbers for different activities (one that you can use for bills, one to register for a Signal account, etc.), and you never need to disable Airplane mode. The benefit of using a data plan instead is that you can use it away from Wi-Fi, at the cost of geolocation (that is, it will be possible for your service provider, and perhaps other parties, to know where your device is at any given time). 

### VPN (Virtual Private Network)

A VPN extends a private network (e.g., your network at home) across a public network (like the Internet). Devices connected to the VPN are part of the private network, even if they are physically somewhere else. Applications using a VPN are subject to the functionality, security, and management of the private network. 

Put another way, it is a technology that essentially makes it appear like you emerge onto the Internet from the network of the company providing the service, rather than from your home network. Your connection to the company is through an encrypted ‘tunnel’. A VPN is not the best tool for anonymity (defined as knowledge of who you are – Tor is far better), but can partially bolster your privacy (defined as knowledge of what you are doing). 

It is important to stress this to cut through the widespread marketing hype; [a VPN is not enough to keep you anonymous](https://www.ivpn.net/privacy-guides/will-a-vpn-protect-me/). Using a VPN can be thought of as simply shifting your trust from a local Internet Service Provider guaranteed to be a snitch to a remote one that claims to put limits on their ability to effectively snitch on you.  

For an excellent comparison of a VPN and [Tor](#tor-network), see [Defend Dissent: Anonymous Routing](https://open.oregonstate.education/defenddissent/chapter/anonymous-routing/).

### Vulnerability

Commonly, vulnerabilities are [exploitable](#exploit) security flaws in software or hardware. Well-known vulnerabilities receive names like Heartbleed, Shellshock, Spectre, or Stagefright and at least one [CVE](#cve) identifier. There aren’t always exploits available for vulnerabilities. A well-known system to classify the severity of vulnerabilities is [CVSS](https://en.wikipedia.org/wiki/Common_Vulnerability_Scoring_System).

### Zero-day exploit

A zero-day [exploit](#exploit) is unknown to the public, publisher, or other parties who would typically mitigate it. As a result, it is extremely powerful and highly-valued. Governments can either develop their own zero-day exploits, or buy them from a [zero-day broker](https://www.wired.com/story/untold-history-americas-zero-day-market/). 
