+++
title = "Recommendations"
sort_by = "date"
paginate_by = 5
+++
<br>

These recommendations are intended for all anarchists and are accompanied by tutorials to put them into practice. They are informed by a threat model protecting against government security forces and equivalent adversaries that are trying to achieve [targeted digital surveillance](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance.html) for [incrimination](https://www.csrc.link/threat-library/tactics/incrimination.html) or [network mapping](https://www.csrc.link/threat-library/techniques/network-mapping.html). The goal is to obscure the visibility of our enemies into our lives and projects, and to leave no traces when this is relevant. Technology is hostile terrain. 

## Phones

>**[Operating system](/glossary#operating-system-os)**: **GrapheneOS** is the only reasonably secure choice for cellphones. See [GrapheneOS for Anarchists](/posts/grapheneos/). Better yet, [don't have a phone](/posts/nophones/). 

Google Pixel phones are the only devices that currently meet GrapheneOS's [hardware security requirements](https://grapheneos.org/faq#device-support). If this is not possible for you, [DivestOS](https://www.privacyguides.org/en/android/#divestos) has more [supported devices](https://divestos.org/pages/devices) and it is significantly better than stock Android. 

## Computers  

>**[Operating system](/glossary#operating-system-os)**: **Tails** is unparalleled for sensitive computer use (writing and sending communiques, moderating a sketchy website, research for actions, reading articles that may be criminalized, etc.). Tails runs from a USB drive, and is [designed](https://tails.boum.org/about/index.en.html) with the anti-forensic property of leaving no trace of your activity on your computer, as well as to force all Internet connections through the [Tor network](/glossary#tor-network). See [Tails for Anarchists](/posts/tails/) and [Tails Best Practices](/posts/tails-best/).

>**[Operating system](/glossary#operating-system-os)**: **Qubes OS** has better security than Tails for many use cases, but has more of a learning curve and no anti-forensic properties. It is nonetheless accessible enough for journalists and other non-technical users. Fundamental knowledge of using Linux is required - see [Linux Essentials](/posts/linux). Qubes OS can even run Windows programs like Adobe InDesign, although much more securely than a standard Windows computer. See [Qubes OS for Anarchists](/posts/qubes/). 

See [When to Use Tails vs Qubes OS](/posts/qubes/#when-to-use-tails-vs-qubes-os)

We do not offer "harm reduction" advice for Windows or macOS computers, because this is already prevalent and gives a false sense of privacy and security. If you need to use one of these Operating Systems, see The Hitchhiker’s Guide to Online Anonymity for tutorials on [Windows](https://anonymousplanet.org/guide.html#windows-host-os) and [macOS](https://anonymousplanet.org/guide.html#macos-host-os). 

## Home Network 
>**[Operating system](/glossary#operating-system-os) (router)**: [**OpenWrt**](https://openwrt.org/). [GL-iNet](https://www.gl-inet.com/) sells affordable OpenWrt routers that are user friendly - the 'Travel' models are sufficient for an apartment-sized residence. 

>**[Operating system](/glossary#operating-system-os) (hardware firewall)**: [**OPNsense**](https://opnsense.org/). Although you can get by with only a router, a hardware firewall allows you to further segment your network, and other security upgrades.    

If an adversary compromises your router, [they can then compromise all devices connecting to it](https://arstechnica.com/information-technology/2022/06/a-wide-range-of-routers-are-under-attack-by-new-unusually-sophisticated-malware/), so it's important to not use the [closed-source](/glossary#open-source) router your Internet Service Provider gives you. Guide forthcoming.  

## Encrypted Messaging

See [Encrypted Messaging for Anarchists](/posts/e2ee/)

## Storing Electronic Devices

See [Making Your Electronics Tamper-Evident](/posts/tamper/).

## A Note On Borders 

If you risk bringing the phone or computer you use in daily life with you across a border (not recommended), ensure that Full Disk Encryption is enabled, it is powered down, and be prepared to refuse password access along with the implications that will have on your border crossing. It is not sufficient to delete files (or messages, etc.) prior to crossing and then unlock your device. In fact, recently deleted files or messages is the first place a border team will look. To prevent data recovery, you must do a Factory Reset and then reinstall the operating system.

A better approach is to have a device dedicated to travel which you can unlock for border agents, because you always cross with a fresh operating system installation. Stock Android can be [easily installed (flashed)](https://flash.android.com) to [certain phone models](https://source.android.com/docs/setup/build/flash#device-requirements), after a Factory Reset. The fresh phone can be populated with benign contacts like family. Once you are across, you can use the phone normally, but redo the flashing before another border crossing. If the device is taken out of your sight at any point don’t even turn it on again before trashing it, as it may now send your password to the agency and be infected with spyware. 

