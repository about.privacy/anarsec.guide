+++
sort_by = "date"
paginate_by = 10 
+++
<br><center>
<h3><b><a href="https://theanarchistlibrary.org/library/return-fire-vol-4-supplement-caught-in-the-net">Technology is a weapon used against us by the network of domination,</a><br> but maybe we can make the blade cut both ways.</b></h3>

---

* Want a quick overview of our advice for all comrades? [**See our recommendations**](/recommendations).
* Don't know where to start? [**Tails for Anarchists**](/posts/tails/) is the guide with the most relevance to all anarchists. All incriminating digital activities should be accomplished with Tails (such as action research or writing communiques). 
* You can also check out a [**series of articles**](/series) or pick a [**tag**](/tags) that interests you.
