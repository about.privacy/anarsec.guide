+++
title = "Search"
sort_by = "date"
paginate_by = 5
+++
<br><center>
The search feature is powered by DuckDuckGo -- a search engine that respects user privacy.
<form action="https://duckduckgo.com/" method="get">
  <input type="hidden" name="sites" value="anarsec.guide">
  <input type="search" name="q">
  <input type="submit" value="Search">
</form>
